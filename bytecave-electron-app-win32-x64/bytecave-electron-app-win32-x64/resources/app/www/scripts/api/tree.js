var fs = require('fs'),
    path = require('path');

//Walk through directory to fetch level one sub-directories only
exports.getDir = function(srcpath) {

  var dirArray = [];

  try {
    var readDir = fs.readdirSync(srcpath).filter(function (file) {
      return fs.statSync(path.join(srcpath, file)).isDirectory();
    });

    //Create JSON for each sub-directory: map to PrimeUI TreeControl
    for (var dir in readDir) {
      var dirObject = {
        label: readDir[dir],
        data: path.normalize(srcpath + path.sep + readDir[dir] + path.sep),
        path: path.normalize(srcpath + path.sep +readDir[dir] + path.sep)
      };
      //Create a JSON array of all the sub-directories in the given directory
      dirArray.push(dirObject);
    }

    //Return the JSON array
    return {
      error: false,
      message: 'Directory fetched',
      data: dirArray
    };
  }

  //Handle errors in case of wrong directory name or permission issues
  catch (err) {
    var message = 'Failed to load directory';
    if (err) {
      if(err.code == 'ENOENT') {
        message = 'No such file or directory';
      }
      return {
        error: true,
        message: message,
        data: null
      };
    }
  }
};