var fs = require('fs');

exports.save = function(path,data) {

    if(path==undefined || data==undefined || data=='') {
        return {
      error: true,
      message: 'Unable to save text',
      data: null
    }
    }
        
  var filepath = path + 'testapp.txt';
  try {
    fs.writeFileSync(filepath,data);
    return {
      error: false,
      message: 'Text saved successfully'
    }
  }
  catch(err) {
    return {
      error: true,
      message: 'Unable to save text'
    }
  }
};