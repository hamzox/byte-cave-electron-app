var fs = require('fs');

exports.load = function(path) {
    if(path==undefined) {
        return {
      error: true,
      message: 'Unable to save text',
      data: null
    }
    }
    
    
  var filepath = path + 'testapp.txt';
  try {
    var data = fs.readFileSync(filepath);
    return {
      error: false,
      message: 'Text loaded successfully',
      data: data.toString()
    }
  }
  catch(err) {
    return {
      error: true,
      message: 'Unable to load text',
      data: null
    }
  }
};