var dt = require('./tree.js');
var ds = require('./savefile.js');
var dl = require('./loadfile.js');

module.exports = {
  fetch: dt.getDir,
  save: ds.save,
  load: dl.load
};