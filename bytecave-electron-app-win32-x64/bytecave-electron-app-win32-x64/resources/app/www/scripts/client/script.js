// Node.js API for the project
var api = require('./scripts/api/api.js');

// Global variables
var directoryInput;
var textArea;
var saveButton;
var loadButton;
var timeOutAlert = 10;
var dirPath;
var loadPath;
// EO - Global variables

// Document on load function
$(function () {
  directoryInput = $('#directory-path');
  textArea = $('#text-input');
  saveButton = $('#save');
  loadButton = $('#load');

  // Creating  Prime UI region
  $('#notification').puigrowl(); //Initiate Prime UI Notification
  saveButton.puibutton(); //Initiate Prime UI Save Button
  loadButton.puibutton(); //Initiate Prime UI Load Button
  directoryInput.puibutton(); //Initiate Prime UI Select button
  textArea.puiinputtextarea({
    counter: $('#text-input-count'),
    counterTemplate: '{0} characters remaining.', //For displaying through expression to the id of display
    maxlength: 1000 //Maximum 1000 characters limit in textarea
  });
  // EO - Creating  Prime UI region

  // Open body and disable save button
  $('body').show();
  disableSaveButton();

  // Setup all event handlers
  textArea.on("keyup", textArea_keyup);
  saveButton.on("click", saveButton_click);
  loadButton.on("click", loadButton_click);
  directoryInput.on('change', directoryInput_change);
  // EO - Setup all event handlers

}); // EO - Document on load function


// EH | Text area key up
function textArea_keyup() {
  if (textArea.val() != "") {
    enableSaveButton(); // Enable save button when text area has a length greater then 0
  }
  else if (textArea.val() == "") {
    disableSaveButton(); // Disable save button when above condition is not true
  }
} // EO - EH | Text area key up

// EH | Save button click
function saveButton_click() {
  if (loadPath == undefined) {
    toast.error("No directory specified");
    return;
  }
  if (!api.save(loadPath, textArea.val()).error) {
    toast.saveFileSuccess(); // Notify, when file save successfully
  }
  else {
    toast.saveFileError(); // Notify, when error occurs
  }
} // EO - EH | Save button click

// EH | Load button click
function loadButton_click() {
  if (loadPath == undefined) {
    toast.error("No directory specified");
    return;
  }
  var res = api.load(loadPath); // Load API from api.js
  if (!res.error) {
    toast.success(res.message); // Notify, when loading is done successfully
    textArea.val(res.data); // Load data from testapp.txt
    enableSaveButton();
  }
  else {
    toast.error(res.message); // Notify, when error occurs in loading
    disableSaveButton();
    textArea.val("");
  }
} // EO - EH | Load button click

// EH | Directory input change
function directoryInput_change(e) {
  dirPath = e.target.files[0].path;
  loadPath = undefined;
  var res = api.fetch(dirPath); // Fetch directory path
  if (res.error) {
    toast.error(res.message); // Notify, when error occur in fetching directory
  }
  else {
    toast.success(res.message); // Notify, when directory is fetched
    $('#tree-container').html('<div class="margin-auto" id="tree"></div>'); // Create directory tree on DOM
    // Create Prime UI Tree
    $('#tree').puitree({
        animate: true,
        selectionMode: 'single',
        lazy: true,
        nodes: function (ui, response) {
          if (ui.node) { // Check if node exist (This will be false for the first time)
            var nodeData = api.fetch(ui.data);
            response.call(this, nodeData.data, ui.node);

            // Apply a jquery Fix to remove the folder open icon when reached a terminal node
            if (nodeData.data.length <= 0) {
              var $node = $(ui.node[0]);
              $node.find('.ui-tree-toggler').removeClass('fa-caret-down');
              $node.find('.ui-treenode-icon').removeClass('fa-folder-open').addClass('fa-folder');
            } // EO -  Apply a jquery Fix to remove the folder open icon when reached a terminal node
          }
          else {
            response.call(this, res.data, ui.node); // Since no node path was found in previous condition add the directory listing from the first api call
          }
        },
        icons: {
          def: {
            expanded: 'fa-folder-open',
            collapsed: 'fa-folder'
          }
        },

        nodeSelect: function (event, node) {
          loadPath = node.data; // Select Node path and save it to global variable
        }
      } // EO - Create Prime UI Tree
    );
  }
} // EO - EH | Directory input change

// FN | Message notifications
function showMessage(msg) {
  $('#notification').puigrowl('option', {"life": 1000 * timeOutAlert}); // Hide notification message on 10 seconds
  $('#notification').puigrowl('show', msg);
}

function disableSaveButton() {                // Disable save button
  saveButton.attr("disabled", "disabled");
  saveButton.addClass("ui-state-disabled");
}

function enableSaveButton() {                 // Enable save button
  saveButton.removeClass("ui-state-disabled");
  saveButton.removeAttr("disabled");
} //EO - FN | Message notifications

// OBJ | Toasts
var toast = {
  saveFileError: function () {
    showMessage([{severity: 'error', summary: 'Error', detail: 'Unable to save text'}]);
  },
  saveFileSuccess: function () {
    showMessage([{
      severity: 'info',
      summary: 'Success',
      detail: 'Text saved successfully'
    }]);
  },
  error: function (msg) {
    showMessage([{severity: 'error', summary: 'Error', detail: msg}]);
  },
  success: function (msg) {
    showMessage([{severity: 'info', summary: 'Success', detail: msg}]);
  }
}; // EO - OBJ | Toasts



